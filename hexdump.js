#!/usr/bin/env node

process.stdin.pause();
const charMap = [...seq(0, 31, () => '.'),
...seq(32, 126, c => String.fromCharCode(c)),
  '.',
...seq(128, 255, c => '.'),
];

let groups = 2;
let groupSize = 8;
let intraHexSeparator = ' ';
let intraAsciiSeparator = '';
let interHexGroupSeparator = '  ';
let interAsciiGroupSeparator = '';
let positionGutter = '  ';
let asciiGutter = '  |';
let lineEnd = '|\n';
let positionWidth = 8;
let positionColour = 2;
let hexColour = 37;
let asciiColour = 37;


function* getData() {
  for (let group = 0; group < groups; group++) {
    yield process.stdin.read(groupSize);
  }
}

let pos = 0;
process.stdin.on('readable', () => {
  let data = [...getData()];
  while (data[0]) {
    data = data.map(x => x || []);

    let out = '\x1b[' + positionColour + 'm'  + leftPad(pos.toString(16), '0', positionWidth) + '\x1b[0m';
    out += positionGutter;
    for (let group = 0; group < groups; group++) {
      if (group > 0) {
        out += interHexGroupSeparator;
      }
      out += [...data[group]].map(d => leftPad(d.toString(16), '0', 2)).join(intraHexSeparator);
    }
    out += asciiGutter;
    for (let group = 0; group < groups; group++) {
      if (group > 0) {
        out += interAsciiGroupSeparator;
      }
      out += [...data[group]].map(d => charMap[d]).join(intraAsciiSeparator);
    }
    process.stdout.write(out + lineEnd);
    pos += data.reduce((acc,curr)=>acc + curr.length, 0);

    data = [...getData()];
    }
});
function leftPad(str, pad, len) {
  return pad.repeat(len - str.length) + str;
}
function* seq(start, end, func) {
  for (let val = start; val <= end; val++) {
    yield func ? func(val) : val;
  }
}
I needed a hexdump tool, and I didn't have one.
I could have hunted for a windows binary, but then I figure I could just as easily write my own fairly quickly.

So I did.

Here it is.

It probably isn't the fastest hexdump ever there was, but at least it should be reasonably cross-platform.

Currently there are no cmdline options at all, it just takes stdin and gives stdout

`cat file | hexdump.js | less`